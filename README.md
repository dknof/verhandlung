# Verhandlung

Test of strategies for the Verhandlung of the game Forge of Empires

## Getting Started

* Get the sourcecode: `git clone https://gitlab.com/dknof/verhandlung.git`
* `cd src`
* Call `make` to build verhandlung

## Examples

### All solutions
```
verhandlung
```

### All solutions after first bid (all resources from left to right)
```
verhandlung rwwff
```
* `r` stands for *r*ight solution
* `w` stands for right resource, but *w*rong person
* `f` stands for *f*alse resource

### All solutions after first bid (all resources from left to right)
```
verhandlung rwf..
```
* `r` stands for *r*ight solution
* `w` stands for right resource, but *w*rong person
* `f` stands for *f*alse resource
* `.` stands for any resource (according to other replies, that is here not resource 2) without further information

## Authors

* Dr. Diether Knof <dknof+verhandlung@posteo.de>

## Contributing

Verhandlung is an open source project. Feel free to contact the [author](mailto:dknof+verhandlung@posteo.de).

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
