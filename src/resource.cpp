#include "constants.h"
#include "resource.h"
#include "negotiant.h"

Resources resources_list = Resources({Resource(0b000000000001000000000001000000000001000000000001000000000001)
                                     ,Resource(0b000000000010000000000010000000000010000000000010000000000010)
                                     ,Resource(0b000000000100000000000100000000000100000000000100000000000100)
                                     ,Resource(0b000000001000000000001000000000001000000000001000000000001000)
                                     ,Resource(0b000000010000000000010000000000010000000000010000000000010000)
                                     ,Resource(0b000000100000000000100000000000100000000000100000000000100000)
                                     ,Resource(0b000001000000000001000000000001000000000001000000000001000000)
                                     ,Resource(0b000010000000000010000000000010000000000010000000000010000000)
                                     ,Resource(0b000100000000000100000000000100000000000100000000000100000000)
                                     ,Resource(0b001000000000001000000000001000000000001000000000001000000000)
                                     });

void shrink_resources_list_according_to_bid(string const bid)
{
  assert(bid.length() == num_negotiants);
  size_t max_num_resources = 0;
  for (size_t i = 0; i < num_negotiants; ++i) {
    if (!std::isdigit(static_cast<unsigned char>(bid[i])))
      invalid_arguments();
    max_num_resources = std::max(max_num_resources, static_cast<size_t>(bid[i] - '0' + 1));
  }
  if (max_num_resources > resources_list.size())
    invalid_arguments();
  cout << "Setting number resources to " << max_num_resources << ".\n";
  while (resources_list.size() > max_num_resources)
    resources_list.pop_back();
  resources_list.shrink_to_fit();
}

auto to_string(Resource resource)
  -> string
{
  for (size_t i = 0; i < resources_list.size(); ++i) {
    if (resources_list[i] == resource) {
      return std::to_string(i);
    }
  }
  return ".";
}

auto operator<<(ostream& ostr, Resource resource)
  -> ostream&
{
  ostr << to_string(resource);
  return ostr;
}
