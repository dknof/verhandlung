#pragma once

#include "strategy.h"
#include <map>

// take the combination with the most solved

class StrategyRecursive : public Strategy {
  public:
    StrategyRecursive() = default;

  private:
    auto calculated_bid(Responses const& responses) const -> Bid final;
};
