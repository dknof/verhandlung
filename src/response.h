#pragma once

#include "reply.h"
#include "combination.h"
class Bid;

class Response {
  public:
    Response(Combination combination, Bid bid);
    Response(Bid bid, string response);
    Response(Bid bid, char const response[num_negotiants + 1]);

    auto bits(Reply reply)  const -> Bitset;
    auto count(Reply reply) const -> size_t;

    auto operator[](Negotiant negotiant) const -> Reply;

  public:
    Bitset solved_bits            = 0; // the solved part of the bid
    Bitset failed_bits            = 0; // the failed part of the bid
    Bitset wrong_bits             = 0; // the wrong part of the bid
    Bitset failed_resources_bits  = 0; // the resources which have failed (| operated)
};

bool operator<(Response const& lhs, Response const& rhs);
auto to_string(Response const& response) -> string;
auto operator<<(ostream& ostr, Response const& response) -> ostream&;


#include "response.hpp"
