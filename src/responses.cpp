#include "constants.h"
#include "response.h"
#include "responses.h"
#include "combination.h"
#include "bid.h"
#include <bitset>

void Responses::add(Response const& response)
{
  assert ((solved_bits & response.solved_bits) == solved_bits);
  assert ((failed_bits & (response.wrong_bits | response.failed_resources_bits)) == 0);
  auto const new_solved_bits = response.solved_bits & ~solved_bits;
  solved_bits |= response.solved_bits;
  failed_bits |= response.failed_resources_bits;
  single_wrong_bits |= response.wrong_bits;
  if (new_solved_bits != 0) {
    for (auto& w : wrong_bits) {
      for (auto const r : resources_list) {
        if ((new_solved_bits & r) != 0) {
          w &= ~r;
        }
      }
    }
  }
  wrong_bits.push_back(response.wrong_bits);
  return ;
}

auto operator<<(ostream& ostr, Responses const& responses)
  -> ostream&
{
  ostr << "solved bids: " << static_cast<std::bitset<64>>(responses.solved_bits) << '\n';
  ostr << "failed bids: " << static_cast<std::bitset<64>>(responses.failed_bits) << '\n';
  ostr << "swrong bids: " << static_cast<std::bitset<64>>(responses.single_wrong_bits) << '\n';
  for (auto const w : responses.wrong_bits) {
    ostr << "wrong bids:  " << static_cast<std::bitset<64>>(w) << '\n';
  }
  return ostr;
}
