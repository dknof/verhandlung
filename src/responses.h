#pragma once

class Response;

struct Responses {
  Bitset solved_bits = 0;    // the solved part of the bid
  Bitset failed_bits = 0;    // the resources which have failed (| operated)
  Bitset single_wrong_bits = 0; // the wrong part of the bid
  vector<Bitset> wrong_bits; // the wrong part of the bid

  bool empty() const;
  auto size() const -> size_t;
  void add(Response const& response);
};

bool operator<(Responses const& lhs, Responses const& rhs);
auto operator<<(ostream& ostr, Responses const& responses) -> ostream&;

#include "responses.hpp"
