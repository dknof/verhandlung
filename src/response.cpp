#include "constants.h"
#include "response.h"
#include "combination.h"
#include "bid.h"

Response::Response(Combination combination, Bid bid) :
  solved_bits(combination & bid)
{
  if (solved_bits == combination.bits)
    return ;

  auto const open_combination_bits = combination.bits & ~solved_bits;
  wrong_bits = bid.bits() & ~solved_bits;
  for (auto const r : resources_list) {
    if (   (open_combination_bits & r) == 0
        && (wrong_bits            & r) != 0) {
      failed_bits           |= wrong_bits & r;
      failed_resources_bits |= r;
      wrong_bits            &= ~r;
    }
  }
  failed_resources_bits &= ~solved_bits;
  return ;
}

Response::Response(Bid const bid, string const response)
{
  assert(response.size() == num_negotiants);
  for (size_t i = 0; i < num_negotiants; ++i) {
    if (response[i] != static_cast<char>(Reply::solved))
      continue;
    auto const n = negotiants_list[i];
    solved_bits |= bid.bits() & n;
  }
  wrong_bits = bid.bits() & ~solved_bits;
  for (size_t i = 0; i < num_negotiants; ++i) {
    if (response[i] != static_cast<char>(Reply::failed))
      continue;
    auto const n = negotiants_list[i];
    failed_bits |= bid.bits() & n;
    failed_resources_bits |= bid[n];
    wrong_bits &= ~n;
  }
  failed_resources_bits &= ~solved_bits;
  return ;
}

Response::Response(Bid const bid, char const response[num_negotiants + 1]) :
  Response(bid, string(response))
{ }

auto to_string(Response const& response)
  -> string
{
  string text;
  for (auto const n : negotiants_list)
    text += to_string(response[n]);
  return text;
}

auto operator<<(ostream& ostr, Response const& response)
  -> ostream&
{
  ostr << to_string(response);
  return ostr;
}
