#include "constants.h"
#include "strategy_simple.h"
#include "response.h"
#include "responses.h"
#include "bid.h"

auto StrategySimple::calculated_bid(Responses const& responses) const
-> Bid
{
  if (responses.empty())
    return Bid({0, 1, 2, 3, 4});

  for (auto const c : all_combinations) {
    if (combination_possible(c, responses)) {
      return c;
    }
  }
  cerr << "no combination is possible\n";
  assert(false);
  std::exit(EXIT_FAILURE);
}
