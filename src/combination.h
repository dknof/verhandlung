#pragma once

#include "negotiant.h"
#include "resource.h"
class Bid;
class Response;
class Responses;

struct Combination {
  Bitset bits = 0;

  Combination() = default;
  Combination(char resources[num_negotiants]);
  Combination(char const resources[num_negotiants]);
  Combination(vector<int> resources);

  operator Bitset() const;
};
using Combinations = vector<Combination>;
extern Combinations all_combinations;
auto generate_all_combinations() -> Combinations;

bool operator==(Combination lhs, Combination rhs);
bool operator!=(Combination lhs, Combination rhs);

auto operator&(Combination lhs, Combination rhs)                 -> Bitset;
auto operator&(Combination combination, Negotiant negotiant)     -> Resource;
auto operator&(Combination combination, Bitset bitset)           -> Bitset;
auto to_string(Combination combination) -> string;
auto operator<<(ostream& ostr, Combination  combination)         -> ostream&;
auto operator<<(ostream& ostr, Combinations const& combinations) -> ostream&;

bool combination_possible(Combination combination, Response const& response);
bool combination_possible(Combination combination, Responses const& responses);
auto possible_combinations(Combinations const& combinations, Response const& response)   -> Combinations;
auto possible_combinations(Combinations const& combinations, Responses const& responses) -> Combinations;
auto count_possible_combinations(Combinations const& combinations, Response const& response)   -> size_t;
auto count_possible_combinations(Combinations const& combinations, Responses const& responses) -> size_t;
auto possible_combinations(Response const& response)   -> Combinations;
auto possible_combinations(Responses const& responses) -> Combinations;


#include "combination.hpp"
