#pragma once

enum class Reply : char {
  empty        = '.',
  solved       = 'r',
  failed       = 'f',
  wrong_person = 'w',
};

auto to_string(Reply reply) -> string;
auto operator<<(ostream& ostr, Reply reply) -> ostream&;
