// to be included by response.h

inline auto Response::bits(Reply const reply) const
-> Bitset
{
  switch (reply) {
  case Reply::empty:
    return ~0;
  case Reply::solved:
    return solved_bits;
  case Reply::failed:
    return failed_bits;
  case Reply::wrong_person:
    return wrong_bits;
  }
  return 0;
}

inline auto Response::count(Reply const reply) const
-> size_t
{
  auto const b = bits(reply);
  return count_if(negotiants_list, [b](auto const n) { return (b & n) != 0; });
}

inline auto Response::operator[](Negotiant const negotiant) const
  -> Reply
{
  if (solved_bits & negotiant)
    return Reply::solved;
  if (failed_bits & negotiant)
    return Reply::failed;
  if (wrong_bits & negotiant)
    return Reply::wrong_person;
  return Reply::empty;
}

inline bool operator<(Response const& lhs, Response const& rhs)
{
  if (lhs.solved_bits < rhs.solved_bits)
    return true;
  if (lhs.solved_bits > rhs.solved_bits)
    return false;
  if (lhs.failed_bits < rhs.failed_bits)
    return true;
  if (lhs.failed_bits > rhs.failed_bits)
    return false;
  if (lhs.wrong_bits < rhs.wrong_bits)
    return true;
  if (lhs.wrong_bits > rhs.wrong_bits)
    return false;
  return false;
}
