#pragma once

enum Negotiant : int_fast64_t {
  negotiant_0 = 0b000000000000000000000000000000000000000000000000111111111111,
  negotiant_1 = 0b000000000000000000000000000000000000111111111111000000000000,
  negotiant_2 = 0b000000000000000000000000111111111111000000000000000000000000,
  negotiant_3 = 0b000000000000111111111111000000000000000000000000000000000000,
  negotiant_4 = 0b111111111111000000000000000000000000000000000000000000000000,
};
auto constexpr negotiants_list = std::experimental::make_array<Negotiant>(negotiant_0,
                                                                          negotiant_1,
                                                                          negotiant_2,
                                                                          negotiant_3,
                                                                          negotiant_4
                                                                         );
auto constexpr num_negotiants  = negotiants_list.size();
