inline bool Responses::empty() const
{
  return wrong_bits.empty();
}

inline auto Responses::size() const
-> size_t
{
  return wrong_bits.size();
}

inline bool operator<(Responses const& lhs, Responses const& rhs)
{
  if (lhs.solved_bits < rhs.solved_bits)
    return true;
  if (lhs.solved_bits > rhs.solved_bits)
    return false;
  if (lhs.failed_bits < rhs.failed_bits)
    return true;
  if (lhs.failed_bits > rhs.failed_bits)
    return false;
  if (lhs.single_wrong_bits < rhs.single_wrong_bits)
    return true;
  if (lhs.single_wrong_bits > rhs.single_wrong_bits)
    return false;
  return (lhs.wrong_bits < rhs.wrong_bits);
}
