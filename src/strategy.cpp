#include "constants.h"
#include "strategy.h"
#include "response.h"
#include "responses.h"
#include "bid.h"

Strategy::Strategy() :
  bids_cache_(5)
{ }

void Strategy::add_bid(Bid bid)
{
  static_bids_.push_back(bid);
}

auto Strategy::bid() const
-> Bid
{
  return bid({});
}

auto Strategy::bid(Responses const& responses) const
-> Bid
{
  if (responses.size() < static_bids_.size())
    return static_bids_[responses.size()];
  if (responses.empty())
    return Bid({0, 1, 2, 3, 4});

  if (responses.size() < bids_cache_.size()) {
    auto const& bids_cache = bids_cache_[responses.size()];
    auto const p = bids_cache.find(responses);
    if (p != end(bids_cache)) {
      return p->second;
    }
  }
  auto const bid = calculated_bid(responses);

  if (responses.size() < bids_cache_.size()) {
    auto& bids_cache = bids_cache_[responses.size()];
    bids_cache.insert({responses, bid});
  }
  return bid;
}
