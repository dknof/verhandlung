#pragma once

#include "strategy.h"
#include <map>

// take the combination with the most combinations solved in the next step

class StrategyMaxSolved : public Strategy {
  public:
    StrategyMaxSolved() = default;

  private:
    auto calculated_bid(Responses const& responses) const -> Bid final;
};
