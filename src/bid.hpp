// to be included by bid.h

inline Bid::Bid(vector<int> resources) :
  Bid(Combination(resources))
{ }

inline Bid::Bid(Combination const combination) :
  combination_(combination)
{ }

inline auto Bid::bits() const
-> Bitset
{ return combination_.bits; }

inline Bid::operator Combination() const
{
  return combination_;
}

inline void Bid::set(Negotiant negotiant, Resource resource)
{
  combination_.bits &= ~negotiant;
  combination_.bits |= (negotiant & resource);
}

inline auto Bid::operator[](Negotiant const negotiant) const
-> Resource
{
  auto const resource_bit = combination_ & negotiant;
  for (auto const r : resources_list) {
    if (resource_bit & r)
      return r;
  }
  cerr << "no resource found\n";
  assert(false);
  exit(EXIT_FAILURE);
}

inline bool operator==(Combination const& combination, Bid const& bid)
{
  return combination == static_cast<Combination>(bid);
}

inline auto operator&(Combination const combination, Bid const bid)
  -> Bitset
{
  return combination.bits & bid.bits();
}

inline auto operator&(Bid const bid, Bitset const bitset)
  -> Bitset
{
  return bid.bits() & bitset;
}

inline auto operator&(Bitset const bitset, Bid const bid)
  -> Bitset
{
  return bitset & bid.bits();
}

inline auto operator|(Bid const bid, Bitset const bitset)
  -> Bitset
{
  return bid.bits() & bitset;
}

inline auto operator|(Bitset const bitset, Bid const bid)
  -> Bitset
{
  return bitset | bid.bits();
}

