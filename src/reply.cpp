#include "constants.h"
#include "reply.h"

auto to_string(Reply reply)
  -> string
{
  return string(1, static_cast<char>(reply));
}

auto operator<<(ostream& ostr, Reply reply)
  -> ostream&
{
  ostr << static_cast<char>(reply);
  return ostr;
}
