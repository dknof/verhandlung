#include "constants.h"
#include "strategy_max_solved.h"
#include "response.h"
#include "responses.h"
#include "bid.h"
#include <set>
using std::set;

namespace {
auto count_solved_combinations(Combinations const& combinations, Bid const bid) -> size_t;
}

auto StrategyMaxSolved::calculated_bid(Responses const& responses) const
-> Bid
{
  if (responses.empty())
    return Bid({0, 1, 2, 3, 4});

  auto const combinations = possible_combinations(responses);
  assert(!combinations.empty());
  auto const bids = valid_bids(responses);
  assert(!bids.empty());
  auto best_bid = bids[0];
  size_t best_solved_count = 0;
  for (auto const bid : bids) {
    auto const solved_count = count_solved_combinations(combinations, bid);

    if (solved_count > best_solved_count
        || (   solved_count == best_solved_count
            && combination_possible(bid, responses)) ) {
      best_solved_count = solved_count;
      best_bid = bid;
    }
  }
  return best_bid;
}

namespace {
auto count_solved_combinations(Combinations const& combinations, Bid const bid)
  -> size_t
  {
    vector<size_t> num_combinations(2, 0);
    for (auto const& c : combinations) {
      auto const response = Response(c, bid);
      size_t const n = std::count_if(combinations, [&response](auto const combination) {
                                     return combination_possible(combination, response);
                                     });
      if (num_combinations.size() <= n)
        num_combinations.resize(n + 1, 0);
      num_combinations[n] += 1;
    }
    size_t solved_count = 0;
    for (size_t i = 1; i < num_combinations.size(); ++i)
      solved_count += num_combinations[i] / i;
    return solved_count;
  }
}
