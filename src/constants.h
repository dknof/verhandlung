// ./constants.h
#pragma once

//#define NDEBUG

#include <iostream>
using std::cout;
using std::cerr;
using std::clog;
using std::endl;
using std::ostream;
#include <string>
using std::string;
using namespace std::literals::string_literals;
#include <vector>
using std::vector;
#include <array>
#include <experimental/array>
using std::array;
#include <memory>
using std::unique_ptr;
using std::make_unique;
#include <bitset>
#include <algorithm>
namespace std {
template<class Container, class Value>
constexpr auto count(Container const& container, Value v)
{ return count(begin(container), end(container), v); }
template<class Container, class UnaryPredicate>
constexpr auto count_if(Container const& container, UnaryPredicate p)
{ return count_if(begin(container), end(container), p); }
template<class Value>
constexpr auto find(vector<Value> const& container, Value v)
{ return find(begin(container), end(container), v); }
template<class Value>
constexpr bool contains(vector<Value> const& container, Value v)
{ return (find(container, v) != end(container)); }
}

#include <cassert>

#define CERR cerr << __FILE__ << '#' << __LINE__ << ' '
#define CLOG clog << __FILE__ << '#' << __LINE__ << ' '

using Bitset = int_fast64_t;

constexpr auto pow(size_t x, size_t y) -> size_t
{
  return __builtin_pow(x, y);
}

extern void invalid_arguments();
