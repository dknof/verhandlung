#pragma once

class Bid;
class Response;
class Responses;
#include <map>

// base class

class Strategy {
  public:
    Strategy();

    void add_bid(Bid bid);
    auto bid() const -> Bid;
    auto bid(Responses const& responses) const -> Bid;

  protected:
    virtual auto calculated_bid(Responses const& responses) const -> Bid = 0;

  private:
    vector<Bid> static_bids_;
    mutable vector<std::map<Responses, Bid>> bids_cache_;
};
