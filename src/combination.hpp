// include from combination.h

inline Combination::operator Bitset() const
{
  return bits;
}

inline bool operator==(Combination const lhs, Combination const rhs)
{
  return lhs.bits == rhs.bits;
}

inline bool operator!=(Combination const lhs, Combination const rhs)
{
  return lhs.bits != rhs.bits;
}

inline auto operator&(Combination const lhs, Combination const rhs)
  -> Bitset
{ return (lhs.bits & rhs.bits); }

inline auto operator&(Combination const combination, Negotiant const negotiant)
  -> Resource
{
  auto const resource_bits = combination.bits & negotiant;
  for (auto const r : resources_list) {
    if (resource_bits & r)
      return r;
  }
  cerr << "No ressource found\n";
  assert(false);
  exit(EXIT_FAILURE);
}

inline auto operator&(Combination const combination, Bitset const bitset)
  -> Bitset
{
  return combination.bits & bitset;
}
