#include "constants.h"

#include "strategy_simple.h"
#include "strategy_max_solved.h"
#include "strategy_recursive.h"
#include "combination.h"
#include "bid.h"
#include "response.h"
#include "responses.h"
#include <cstring>
#include <numeric>
#include <iomanip>

void detect_combinations(Strategy const& strategy, Combinations const& combinations);

auto const help_text = "verhandlung\n"
"call\n"
"  verhandlung\n"
"    default solving with max strategy and 5 resources\n"
"  verhandlung --help\n"
"    print this help\n"
"  verhandlung [strategy] [bid] [response] [bid] [response] ...\n"
"    use the strategy to show all combinations with the given bids and responses \n"
"\n"
"Strategie\n"
"* simple: just take the first matching combination\n"
"* max: select the bid that gives the maximal number of solved combinations in the next step\n"
"* recursive: select the bid with gets the most rights in the next bid\n"
"\n"
"Bid\n"
"A bid is five digits from 0 to 9.\n"
"The highest number in the first bid defines the number of resources.\n"
"\n"
"Response\n"
"A respone is five characters of 'r', 'f' or 'w'\n"
"'r' says the bid is right in that position\n"
"'f' says the resource of this bid is in no other positions with 'f' or 'w'\n"
"'w' says the resource must be in another position with 'f' or 'w'\n"
"\n"
"Examples\n"
"  verhandlung 01234\n"
"  verhandlung simple 01234 rrwwf\n"
"  verhandlung 01234 rrwwf 01322\n"
;

void invalid_arguments()
{
  cerr << "Call 'verhandlung --help' for further help.\n";
  exit(EXIT_FAILURE);
}

auto main(int const argc, char* argv[])
  -> int
{
  static_assert(num_negotiants == 5);

  unique_ptr<Strategy> strategy;

  if (argc == 2) {
    if (   argv[1] == "--help"s || argv[1] == "-h"s  || argv[1] == "-?"s) {
      cout << help_text;
      return EXIT_SUCCESS;
    }
  }
  int argi = 1;
  if (argi < argc) {
    if (argv[argi] == "simple"s) {
      strategy = make_unique<StrategySimple>();
      argi += 1;
    } else if (argv[argi] == "max"s) {
      strategy = make_unique<StrategyMaxSolved>();
      argi += 1;
    } else if (argv[argi] == "recursive"s) {
      strategy = make_unique<StrategyRecursive>();
      argi += 1;
    }
  }
  if (!strategy)
      strategy = make_unique<StrategyMaxSolved>();

  if (argi < argc) {
    if (strlen(argv[argi]) != num_negotiants) {
      cerr << "The argument '" << argv[argi] << "' must be of length " << num_negotiants << '\n';
      invalid_arguments();
    }
    shrink_resources_list_according_to_bid(argv[argi]);
  } else {
    shrink_resources_list_according_to_bid("01234");
  }

  ::all_combinations = generate_all_combinations();
  if (argi < argc) { // read the bids and replies
    for (; argi < argc; ++argi) {
      if (string(argv[argi]).length() != num_negotiants) {
        cerr << "The bid '" << argv[argi] << "' must be of length " << num_negotiants << '\n';
        cerr << help_text;
        return EXIT_FAILURE;
      }
      Bid const bid(argv[argi]);
      cout << "Add static bid: " << bid << '\n';
      strategy->add_bid(bid);

      argi += 1;
      if (argi >= argc)
        break;
      if (string(argv[argi]).length() != num_negotiants) {
        cerr << "The response '" << argv[argi] << "' must be of length " << num_negotiants << '\n';
        cerr << help_text;
        return EXIT_FAILURE;
      }
      Response const response(bid, argv[argi]);
      cout << "Add response:   " << response << '\n';
      all_combinations = possible_combinations(response);
    }
  }

  detect_combinations(*strategy, all_combinations);
  return EXIT_SUCCESS;
}

void detect_combinations(Strategy const& strategy, Combinations const& combinations)
{
  vector<unsigned> won(10, 0);
  unsigned lost = 0;
  cout << "Number of combinations: " << combinations.size() << '\n';
  for (auto combination : combinations) {
    cout << combination;
    Bids bids;
    Responses responses;

    for (size_t i = 0; i < won.size(); ++i) {
      cout << "   =" << responses.size() << "= ";
      bids.push_back(strategy.bid(responses));
      cout << bids.back();
      auto const response = Response(combination, bids.back());
      responses.add(response);
      cout << " -> " << response;
      if (bids.back() == combination)
        break;
    }

    cout << "   ==> " << (combination == bids.back() ? "won" : "lost");

    if (combination == bids.back())
      won[bids.size() - 1] += 1;
    else
      lost += 1;

    cout << '\n';
  }

  unsigned wons = 0;
  for (size_t i = 0; i < won.size(); ++i) {
    if (won[i] > 0) {
      wons += won[i];
      cout << i + 1 << " bids: " << std::setw(5) << won[i] << " won -- "
        << "total: " << std::setw(5) << wons << " / " << combinations.size() << "  (" << std::setw(3) << (100 * wons / combinations.size()) << " %)\n";
    }
  }

  return ;
}

