#pragma once

#include "strategy.h"

// return the first possible combination

class StrategySimple : public Strategy {
  public:
    StrategySimple() = default;

  private:
    auto calculated_bid(Responses const& responses) const -> Bid final;
};
