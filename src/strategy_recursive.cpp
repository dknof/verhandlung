#include "constants.h"
#include "strategy_recursive.h"
#include "response.h"
#include "responses.h"
#include "bid.h"
#include <set>
using std::set;

namespace {
auto count_solved_combinations(Combinations const& combinations, Responses const& responses,
                               size_t remaining_depth) -> vector<size_t>;
auto count_solved_combinations(Combinations const& combinations, Responses const& responses, Bid const bid,
                               size_t remaining_depth) -> vector<size_t>;
bool operator<(vector<size_t> const& lhs, vector<size_t> const& rhs);
}

auto StrategyRecursive::calculated_bid(Responses const& responses) const
-> Bid
{
  if (responses.empty())
    return Bid({0, 1, 2, 3, 4});

  auto const combinations = possible_combinations(responses);
  assert(!combinations.empty());
  auto const bids = valid_bids(responses);
  assert(!bids.empty());
  auto best_bid = bids[0];
  vector<size_t> best_solved_count;
  auto best_solved_count_size = resources_list.size() - responses.size();
  for (auto const bid : bids) {
    auto const solved_count = count_solved_combinations(combinations, responses, bid,
                                                        best_solved_count_size);
    if (solved_count.empty())
      continue;

    if (   best_solved_count.empty()
        || solved_count > best_solved_count
        || (   solved_count == best_solved_count
            && combination_possible(bid, responses)) ) {
      best_solved_count = solved_count;
      best_solved_count_size = solved_count.size();
      best_bid = bid;
    }
  }
  return best_bid;
}

namespace {
auto count_solved_combinations(Combinations const& combinations, Responses const& responses,
                               size_t remaining_depth)
  -> vector<size_t>
  {
    auto const combinations2 = possible_combinations(combinations, responses);
    if (combinations2.size() == 1)
      return {1};
    auto const bids = valid_bids(responses);
    vector<size_t> best_solved_count;
    //cout << bids.size() << '\n';
    for (auto const bid : bids) {
      auto const solved_count = count_solved_combinations(combinations2, responses, bid, remaining_depth);

      if (solved_count.empty())
        continue;

      if (   best_solved_count.empty()
          || solved_count < best_solved_count
          || (   solved_count == best_solved_count
              && combination_possible(bid, responses)) ) {
        best_solved_count = solved_count;
        remaining_depth = solved_count.size();
      }
    }
    return best_solved_count;
  }

auto count_solved_combinations(Combinations const& combinations, Responses const& responses, Bid const bid,
                               size_t const remaining_depth)
  -> vector<size_t>
  {
    vector<size_t> num_combinations(1, 0);
    for (auto const c : combinations) {
      if (c == bid) {
        num_combinations[0] += 1;
        continue;
      }
      if (remaining_depth == 0)
        return {};
      auto responses2 = responses;
      responses2.add({c, bid});
      auto const combinations2 = possible_combinations(combinations, responses2);
      if (combinations2.size() == combinations.size())
        continue;
      auto const num_combinations2 = count_solved_combinations(possible_combinations(combinations, responses2),
                                                               responses2, remaining_depth - 1);
      if (num_combinations.size() < num_combinations2.size() + 1)
        num_combinations.resize(num_combinations2.size() + 1);
      for (size_t i = 0; i < num_combinations2.size(); ++i)
        num_combinations[i + 1] += num_combinations2[i];
    }
    return num_combinations;
  }

bool operator<(vector<size_t> const& lhs, vector<size_t> const& rhs)
{
  if (lhs.size() < rhs.size())
    return true;
  if (lhs.size() > rhs.size())
    return false;
  auto const n = lhs.size();
  for (size_t i = 0; i < n; ++i) {
   if (lhs[n - 1 - i] < rhs[n - 1 - i])
     return true;
   if (lhs[n - 1 - i] > rhs[n - 1 - i])
     return false;
  }
  return false;
}
}
