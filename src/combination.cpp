#include "constants.h"
#include "combination.h"
#include "bid.h"
#include "response.h"
#include "responses.h"
#include "negotiant.h"
#include "resource.h"

Combinations all_combinations;

auto generate_all_combinations()
  -> Combinations
{
  auto const num_negotiants = negotiants_list.size();
  auto const num_resources = resources_list.size();
  Combinations combinations(pow(num_resources, num_negotiants));
  for (size_t c = 0; c < combinations.size(); ++c) {
    auto& combination = combinations[c];
    auto x = c;
    for (size_t i = 0; i < num_negotiants; ++i) {
      combination.bits |= negotiants_list[num_negotiants - 1 - i] & resources_list[x % num_resources];
      x /= num_resources;
    }
  }
  return combinations;
}

Combination::Combination(char resources[num_negotiants])
{
  for (size_t i = 0; i < num_negotiants; ++i) {
    if (!std::isdigit(static_cast<unsigned char>(resources[i]))) {
      cerr << "The resources '" << string(resources, 5) << "' must be a only digits\n";
      invalid_arguments();
    }
    size_t const r = resources[i] - '0';
    if (r >= resources_list.size()) {
      cerr << "The resources '" << string(resources, 5) << "' must be a only digits between 0 and " << resources_list.size() - 1 << '\n';
      invalid_arguments();
    }
    bits |= negotiants_list[i] & resources_list[r];
  }
  return ;
}

Combination::Combination(char const resources[num_negotiants])
{
  for (size_t i = 0; i < num_negotiants; ++i) {
    if (!std::isdigit(static_cast<unsigned char>(resources[i]))) {
      cerr << "The resources '" << string(resources, 5) << "' must be a only digits\n";
      invalid_arguments();
    }
    size_t const r = resources[i] - '0';
    if (r >= resources_list.size()) {
      cerr << "The resources '" << string(resources, 5) << "' must be a only digits between 0 and " << resources_list.size() - 1 << '\n';
      invalid_arguments();
    }
    bits |= negotiants_list[i] & resources_list[r];
  }
  return ;
}

Combination::Combination(vector<int> const resources)
{
  assert(resources.size() == num_negotiants);
  for (size_t i = 0; i < num_negotiants; ++i) {
    assert (resources[i] >= 0 && static_cast<size_t>(resources[i]) < resources_list.size());
    bits |= negotiants_list[i] & resources_list[resources[i]];
  }
  return ;
}

auto to_string(Combination const combination)
  -> string
{
  string text;
  for (auto const n : negotiants_list) {
    text += to_string(combination & n);
  }
  return text;
}

auto operator<<(ostream& ostr, Combination const combination)
  -> ostream&
{
  cout << to_string(combination);
  return ostr;
}

auto operator<<(ostream& ostr, Combinations const& combinations)
  -> ostream&
{
  for (auto const& c : combinations)
    ostr << c << '\n';
  return ostr;
}

bool combination_possible(Combination const combination,
                          Response const& response)
{
  if ((combination & response.solved_bits) != response.solved_bits)
    return false;
  if ((combination & response.failed_resources_bits) != 0)
    return false;
  if (response.wrong_bits == 0)
    return true;
  if ((combination & response.wrong_bits) != 0)
    return false;
  for (auto const r : resources_list) {
    if ((response.wrong_bits & r) == 0)
      continue;
    if ((combination & r & ~response.solved_bits) == 0)
      return false;
  }

  return true;
}

bool combination_possible(Combination const combination,
                          Responses const& responses)
{
  if ((combination & responses.solved_bits) != responses.solved_bits)
    return false;
  if ((combination & responses.failed_bits) != 0)
    return false;
  if ((combination & responses.single_wrong_bits) != 0)
    return false;
  for (auto const wrong_bits : responses.wrong_bits) {
    if (wrong_bits == 0)
      continue;
    for (auto const r : resources_list) {
      if ((wrong_bits & r) == 0)
        continue;
      if ((combination & r & ~responses.solved_bits) == 0)
        return false;
    }
  }

  return true;
}

auto possible_combinations(Combinations const& combinations,
                           Response const& response)
  -> Combinations
{
  Combinations result;
  for (auto const& c : combinations) {
    if (combination_possible(c, response))
      result.push_back(c);
  }
  return result;
}

auto possible_combinations(Combinations const& combinations,
                           Responses const& responses)
  -> Combinations
{
  Combinations result;
  for (auto const& c : combinations) {
    if (combination_possible(c, responses))
      result.push_back(c);
  }
  return result;
}

auto possible_combinations(Response const& response)
  -> Combinations
{
  return possible_combinations(all_combinations, response);
}

auto possible_combinations(Responses const& responses)
  -> Combinations
{
  return possible_combinations(all_combinations, responses);
}
