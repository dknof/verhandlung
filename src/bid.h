#pragma once

#include "resource.h"
#include "negotiant.h"
#include "combination.h"
class Response;
class Responses;

class Bid {
  public:
    Bid(vector<int> resources);
    Bid(Combination combination);

    operator Combination() const;
    auto bits() const -> Bitset;
    void set(Negotiant negotiant, Resource resource);
    auto operator[](Negotiant negotiant) const -> Resource;

    bool is_valid(Response const& response) const;
    bool is_valid(Responses const& responses) const;

  private:
    Combination combination_;
};

auto operator<<(ostream& ostr, Bid bid)          -> ostream&;
auto operator<<(ostream& ostr, vector<Bid> const& bids) -> ostream&;

bool operator==(Combination const& combination, Bid const& bid);

auto operator&(Combination combination, Bid bid) -> Bitset;
auto operator&(Bid bid, Bitset bitset) -> Bitset;
auto operator&(Bitset bitset, Bid bid) -> Bitset;
auto operator|(Bid bid, Bitset bitset) -> Bitset;
auto operator|(Bitset bitset, Bid bid) -> Bitset;

using Bids = vector<Bid>;

auto valid_bids(Combinations const& combinations, Response  const& response)  -> vector<Bid>;
auto valid_bids(Combinations const& combinations, Responses const& responses) -> vector<Bid>;
auto valid_bids(Bids const& combinations,         Response  const& response)  -> vector<Bid>;
auto valid_bids(Bids const& combinations,         Responses const& responses) -> vector<Bid>;
auto valid_bids(Response  const& response)  -> vector<Bid>;
auto valid_bids(Responses const& responses) -> vector<Bid>;

#include "bid.hpp"
