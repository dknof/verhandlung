#pragma once

struct Resource {
  Bitset bits;

  constexpr Resource(Bitset b) : bits(b) {}
  constexpr operator Bitset() const { return bits; }
};
using Resources = vector<Resource>;
void shrink_resources_list_according_to_bid(string bid);

extern Resources resources_list;

auto to_string(Resource resource) -> string;
auto operator<<(ostream& ostr, Resource resource) -> ostream&;
