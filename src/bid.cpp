#include "constants.h"
#include "bid.h"
#include "response.h"
#include "responses.h"
#include "combination.h"

bool Bid::is_valid(Response const& response) const
{
  if ((combination_ & response.solved_bits) != response.solved_bits)
    return false;
  if ((combination_ & response.failed_bits) != 0)
    return false;

  return true;
}

bool Bid::is_valid(Responses const& responses) const
{
  if ((combination_ & responses.solved_bits) != responses.solved_bits)
    return false;
  if ((combination_ & responses.failed_bits) != 0)
    return false;

  return true;
}

auto operator<<(ostream& ostr, Bid const bid)
  -> ostream&
{
  ostr << static_cast<Combination>(bid);
  return ostr;
}

auto operator<<(ostream& ostr, vector<Bid> const& bids)
  -> ostream&
{
  for (auto const& b : bids)
    ostr << b << '\n';
  return ostr;
}

auto valid_bids(Combinations const& combinations,
                Response const& response)
  -> vector<Bid>
{
  if (combinations.empty())
    return {};

  vector<Bid> result;
  for (auto const c : combinations) {
    auto const b = Bid(c);
    if (b.is_valid(response))
      result.push_back(b);
  }
  return result;
}

auto valid_bids(Combinations const& combinations,
                Responses const& responses)
  -> vector<Bid>
{
  if (combinations.empty())
    return {};

  vector<Bid> result;
  for (auto const c : combinations) {
    auto const b = Bid(c);
    if (b.is_valid(responses))
      result.push_back(b);
  }
  return result;
}

auto valid_bids(vector<Bid> const& combinations,
                Response const& response)
  -> vector<Bid>
{
  if (combinations.empty())
    return {};

  vector<Bid> result;
  for (auto const c : combinations) {
    auto const b = Bid(c);
    if (b.is_valid(response))
      result.push_back(b);
  }
  return result;
}

auto valid_bids(vector<Bid> const& combinations,
                Responses const& responses)
  -> vector<Bid>
{
  if (combinations.empty())
    return {};

  vector<Bid> result;
  for (auto const c : combinations) {
    auto const b = Bid(c);
    if (b.is_valid(responses))
      result.push_back(b);
  }
  return result;
}

auto valid_bids(Response const& response)
  -> vector<Bid>
{
  return valid_bids(all_combinations, response);
}

auto valid_bids(Responses const& responses)
  -> vector<Bid>
{
  return valid_bids(all_combinations, responses);
}
